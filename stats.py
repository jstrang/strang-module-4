import sys, urllib2, re

class Player:
    def __init__(self, name):
        self.name = name
        self.hits = 0
        self.atbats = 0
        
    def addGame(self, hit, bat):
        self.hits = self.hits+hit
        self.atbats = self.atbats+bat
        
    def stats(self):
        avg = float(self.hits)/float(self.atbats)
        print self.name, ": %.3f" % avg
        
    def getavg(self):
        return float(self.hits)/float(self.atbats)
    
    
if len(sys.argv) < 2:
    sys.exit("Usage: %s pathname" % sys.argv[0])
    
pathname = sys.argv[1]

players = dict()
playerList = []
i = 0
regex = re.compile(r"(\b[A-Z]\w+\b \b[A-Z]\w+\b) batted (\d+) times with (\d+) hits")

file = urllib2.urlopen(pathname)
for line in file:
    match = regex.match(line)
    if match is not None:
        name = match.group(1)
        b = int(match.group(2))
        h = int(match.group(3))
        if name not in players:
            playerList.append(Player(name))
            players[name] = playerList[i]
            i = i+1
        players[name].addGame(h,b)


while players:
    currentMax = 0
    currentBest = None
    for key in players:
        if players[key].getavg() > currentMax:
            currentBest = key
            currentMax = players[key].getavg()
    players[currentBest].stats()
    del players[currentBest]
