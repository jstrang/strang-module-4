(.+?)(hello, world+)(.+)

\b\w*[AEIOUaeiou]{3}\w*\b

^[A-Z]{2}\d{3,4}$